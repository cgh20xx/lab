// 建構函數綁定法

function Animal() {
    this.species = '動物';
}

function Cat(name, color) {
    Animal.apply(this, arguments); // 執行 Animal() 並將 Cat 代入
    this.name = name;
    this.color = color;
    this.type = '貓科';
    this.eat = function() {
        return this.species + ':' + this.name + '吃老鼠'; // 這邊就可以取到 Animal 的 species 特性
    }
}

var animal = new Animal();
var cat1 = new Cat('小明', '白色');
var cat2 = new Cat('大毛', '黑色');


console.log(cat1);
console.log(cat2);
console.log(cat1 instanceof Cat); // true
console.log(cat2 instanceof Cat); // true
console.log(cat2 instanceof Animal); // false


console.log(cat1.eat()); // 動物:小明吃老鼠

console.log(cat1.eat == cat2.eat); // false: cat1 和 cat2 的 eat 方法是不同的記憶體位置(浪費空間)

console.log(cat1.hasOwnProperty('species')); // true: species 在 Animal 為什麼會 true? 因為不是用 prototype 的寫法，每個 instance 都有自已的 species 特性

// 直接繼承 prototype (利用空物件做媒介) 
// 將上一個範例封裝成一個 function ，重覆使用
function extend(Child, Parent) {
    var F = function() {};
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.prototype.parent = Parent.prototype; // 這一行只是為了讓 Child 可以調用到父對象的 prototype，屬備用性質。
}


function Animal() {}

Animal.prototype.species = '動物';

function Cat(name, color) {
    this.name = name;
    this.color = color;
}

extend(Cat, Animal);

var cat1 = new Cat('小明', '白色');
// var cat2 = new Cat('大毛', '黑色');


console.log(cat1);
// console.log(cat2);

console.log(cat1 instanceof Cat); // true
console.log(cat1 instanceof Animal); // true
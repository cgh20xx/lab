// Prototype 繼承模式 
// 如果"猫"的prototype对象，指向一个Animal的实例，那么所有"猫"的实例，就能继承Animal了。

function Animal() {
    console.log("[Call Animal's constructor]");
    this.species = '動物';
}

function Cat(name, color) {
    console.log("[Call Cat's constructor]");
    this.name = name;
    this.color = color;
}

Cat.prototype = new Animal(); 
// 但最後只有實體化 Cat 並無實體化 Animal，Call Animal's constructor 浪費資源
// 且 Cat.prototype.constructor 會因此指到 Animal 這是不合理的，因此要再手動指回 Cat
Cat.prototype.constructor = Cat;

var cat1 = new Cat('小明', '白色');
// var cat2 = new Cat('大毛', '黑色');


console.log(cat1);
// console.log(cat2);

console.log(cat1.species);

console.log('Cat.prototype.isPrototypeOf(cat1):' + Cat.prototype.isPrototypeOf(cat1));

console.log(cat1.__proto__);

console.log(cat1.hasOwnProperty('name')); // true
console.log(cat1.hasOwnProperty('species')); // false 因為不是自身的特性
// console.log('species' in cat1); // true in 運算符可以判斷 instance 是否有某個屬性，不論是不是自身。


console.log(Cat.prototype);

console.log(cat1 instanceof Cat); // true
console.log(cat1 instanceof Animal); // true
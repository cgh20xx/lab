// Prototype模式 

function Cat(name, color) {
    this.name = name;
    this.color = color;
}

Cat.prototype = {
    type: '貓科', // 雖然寫在 prototype 但數值字串布林都還是傳值非傳址
    eat: function() {
        return this.name + '吃老鼠';
    }
};
// 注意 Function 原本的 prototype 內建有 constructor 的屬性 但寫法1用 Cat.prototype = {} 複寫掉後 就沒有 constructor 的屬性了。需將 Cat.prototype.constructor 重新指定到 Cat

var cat1 = new Cat('小明', '白色');
var cat2 = new Cat('大毛', '黑色');

console.log(cat1.eat == cat2.eat); // true: cat1 和 cat2 的 eat 方法指向相同記憶體位置(省空間)


// console.log(cat1);
// console.log(cat2);
// console.log(cat1 instanceof Cat);
// console.log(cat2 instanceof Cat);

console.log('cat1.eat():' + cat1.eat()); // 動物:小明吃老鼠

console.log('Cat.prototype.isPrototypeOf(cat1):' + Cat.prototype.isPrototypeOf(cat1));

console.log(cat1.__proto__);  //  Object { type="貓科",  eat=function()}

console.log(cat1.hasOwnProperty('name')); // true
console.log(cat1.hasOwnProperty('type')); // false 因為不是自身的特性
console.log('type' in cat1); // true in 運算符可以判斷 instance 是否有某個屬性，不論是不是自身。

// 直接繼承 prototype (利用空物件做媒介) 
// 由于"直接继承prototype"存在父類別和子類別的 prototype 都是指向同一個位址的缺點，所以就有第四種方法，利用一个空物件作為中介。

function Animal() {}

Animal.prototype.species = '動物';

function Cat(name, color) {
    this.name = name;
    this.color = color;
}

// 建立空物件F
var F = function() {};
F.prototype = Animal.prototype;

Cat.prototype = new F();
Cat.prototype.constructor = Cat;


var cat1 = new Cat('小明', '白色');
// var cat2 = new Cat('大毛', '黑色');


console.log(cat1);
// console.log(cat2);

console.log(Cat.prototype.constructor); // Cat(name, color)
console.log(Animal.prototype.constructor); // Animal()

// 這時修改 Cat.prototype 就不會影響到 Animal.prototype
Cat.prototype.test = 'tttt';
console.log(Cat.prototype);
console.log(Animal.prototype);

console.log(cat1 instanceof Cat); // true
console.log(cat1 instanceof Animal); // true
// 非建構函數的繼承 深拷貝 目前jQuery使用這方法
// 把父對像的所有屬性拷貝給子對象

function deepCopy(p, c) {
    var c = c || {};

    for (var i in p) {
        if (typeof p[i] === 'object') {
            //如果 property 是物件或陣列
            c[i] = (p[i].constructor === Array) ? [] : {};
            deepCopy(p[i], c[i]);
        } else {
            // 淺拷貝
            c[i] = p[i];
        }
    }

    c.uber = p;

    return c;
}

var animal = {
    species: '動物',
    a: {b: 'c'}
};


var cat = deepCopy(animal);

cat.name = '小明';

console.log(cat); // Object { name="小明",  species="動物"}
console.log(cat.prototype); // undefined
console.log(cat.__proto__); // Object {}


console.log(animal.a.b);  //c
cat.a.b = '不是動物'; // 改子對象a.b
console.log(animal.a.b); // 父對象不會被影響 還是 c
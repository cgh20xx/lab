// 直接繼承 prototype 
// 第三种方法是对第二种方法的改进。由于Animal对象中，不变的属性都可以直接写入Animal.prototype。
// 所以，我们也可以让Cat()跳过 Animal()，直接继承Animal.prototype。

function Animal() {
    console.log("[Call Animal's constructor]");
}

Animal.prototype.species = '動物';

function Cat(name, color) {
    console.log("[Call Cat's constructor]");
    this.name = name;
    this.color = color;
}

// 直接繼承 Animal.prototype 效能較好，因為不用建立 Animal 的實例就不會呼叫 Animal 的 constructor 了。
// 但 Cat.prototype.constructor 還是會因此指到 Animal 這是不合理的，因此要再手動指回 Cat
Cat.prototype = Animal.prototype;
Cat.prototype.constructor = Cat;


var cat1 = new Cat('小明', '白色');
// var cat2 = new Cat('大毛', '黑色');


console.log(cat1);
// console.log(cat2);


// 缺點是 Cat.prototype 和 Animal.prototype 現在都指到同一個對象了，那麼對 Cat.prototype 的修改都會反應到 Animal.prototype
console.log(Cat.prototype.constructor); // Cat(name, color)
console.log(Animal.prototype.constructor); // Cat(name, color)

Cat.prototype.test = 'tttt';
console.log(Animal.prototype); // 看的出來結果是 Cat { species="動物",  test="tttt"}

console.log(cat1 instanceof Cat); // true
console.log(cat1 instanceof Animal); // true


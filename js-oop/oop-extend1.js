// 應該是目前較好的繼承方式
// 延伸 oop-proto5.js 改善父類無法使用 this 特性
// 使用 apply() 繼承父類特性
// 注意：呼叫 extend() 需在定義子類的 prototype 之前，否則子類方法無法 overwrite 父類方法
function extend(Child, Parent) {
    // step1 方法1:
    // var F = function() {};
    // F.prototype = Parent.prototype;
    // Child.prototype = new F();
    
    // step1 方法2:
    Child.prototype = Object.create(Parent.prototype);

    // step2:
    Child.prototype.constructor = Child;
    
    // step3: 為了讓 Child 可以調用到父對象的 prototype，屬備用性質。
    Child.prototype.uber = Parent.prototype;
}


function Animal() {
    this.species = '動物';
}

Animal.prototype.walk = function () {
    console.log(this.species + ' 開始走路');
};

function Cat(name, color) {
    Animal.apply(this, arguments); // 繼承父類的特性 (重要)
    this.species = '貓';
    this.name = name;
    this.color = color;
}

extend(Cat, Animal); //繼承父類的方法

Cat.prototype.walk = function () {
    console.log(this.species + ' 輕快的走路');
}


var cat1 = new Cat('一毛', '白色');

console.log(cat1.walk()); // 貓 開始走路 (子類的 walk method 並沒有 overwrite 父類同名方法)
console.log(cat1 instanceof Cat); // true
console.log(cat1 instanceof Animal); // true
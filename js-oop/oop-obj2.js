// 非建構函數的繼承 淺拷貝 早期jQuery使用這方法
// 把父對像的所有屬性拷貝給子對像

function extendCopy(p) {
    var c = {};

    for (var i in p) {
        c[i] = p[i];
    }

    c.uber = p;

    return c;
}

var animal = {
    species: '動物',
    a: {b: 'c'}
};


var cat = extendCopy(animal);

cat.name = '小明';

console.log(cat); // Object { name="小明",  species="動物"}
console.log(cat.prototype); // undefined
console.log(cat.__proto__); // Object {}


// 但淺拷貝的缺點是父對像的屬性是物件或陣列，那子對象得到是父對象的位址，非值。
console.log(animal.a.b);  //c
cat.a.b = '不是動物'; // 改子對象a.b
console.log(animal.a.b); // 父對象也被更改了！ 改成 不是動物
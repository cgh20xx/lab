// 建構函數綁定法

function A() {
    this.species = '動物';
}

function B(name) {
    A.apply(this, arguments)
    this.name = name;
}

function C(name, color) {
    B.apply(this, arguments)
    this.color = color;
    this.eat = function() {
        console.log(this.name + ' eat mouse');
    }
}

var a = new A();
var b = new B('大毛');
var c = new C('大毛', '白色');
var c1 = new C('小明', '紅色');

console.log(a); // Object { species: "動物" }
console.log(b); // Object { species: "動物", name: "大毛" }
console.log(c); // Object { species: "動物", name: "大毛", color: "白色", eat: C/this.eat() }
console.log(c instanceof B); // false
console.log(c instanceof Object); // true
console.log(c instanceof C); // true
console.log(b instanceof B); // true

console.log(c.constructor); // function C()
console.log(C.constructor); // function Function()

console.log(c.eat()); // 大毛 eat mouse
console.log(c1.eat()); // 小明 eat mouse
console.log(c.eat == c1.eat); // false


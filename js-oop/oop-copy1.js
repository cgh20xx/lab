// 拷貝繼承 prototype (利用空物件做媒介) 
// 將上一個範例封裝成一個 function ，重覆使用
function extend(Child, Parent) {
    var p = Parent.prototype;
    var c = Child.prototype;
    for (var i in p) {
        c[i] = p[i]
    }
    c.uber = p;
}


function Animal() {}

Animal.prototype.species = '動物';

function Cat(name, color) {
    this.name = name;
    this.color = color;
}

extend(Cat, Animal);

var cat1 = new Cat('小明', '白色');
// var cat2 = new Cat('大毛', '黑色');


console.log(cat1);
// console.log(cat2);

console.log(cat1 instanceof Cat); // true
console.log(cat1 instanceof Animal); // false: 缺點是 cat1 卻不是 Animal 的 instance
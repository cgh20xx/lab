// 非建構函數的繼承

function object(o) {
    var F = function() {};
    F.prototype = o;
    return new F();
}

var animal = {
    species: '動物'
};


var cat = object(animal);

cat.name = '小明';

console.log(cat); // Object { name="小明",  species="動物"}
console.log(cat.prototype); // undefined
console.log(cat.__proto__); // Object { species="動物"}
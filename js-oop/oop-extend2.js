// 運用輪循的方式將父類方法寫到子類
// 小缺點：子類 instanceof 父類 == false

function Animal() {
    this.species = '動物';
}

Animal.prototype.walk = function () {
    console.log(this.species + ' 開始走路');
};

function Cat(name, color) {
    Animal.apply(this, arguments); // 繼承到父類的特性
    var prop;
    var proto = this.constructor.prototype;
    for (prop in Animal.prototype) {
        if (!proto[prop]) {
            proto[prop] = Animal.prototype[prop];
        }
        proto[prop]['super'] = Animal.prototype;
    }
    this.species = '貓';
    this.name = name;
    this.color = color;
}

Cat.prototype.dance = function () {
    console.log(this.name + ' 在跳舞');
}

// Cat.prototype.walk = function () {
//     console.log(this.name + ' 輕快的走路');
// }


var cat1 = new Cat('一毛', '白色');

console.log(cat1);
console.log(cat1.walk()); // 貓 開始走路 (呼叫父類方法)
console.log(cat1.dance()); // 一毛 在跳舞 (呼叫自已的方法)

console.log(cat1 instanceof Cat); // true
console.log(cat1 instanceof Animal); // false